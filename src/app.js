import $ from "jquery";


$(function () { /* $(document).ready(function () */


    // globale Variablen
    let todosArray = getLocalStorage();
    let todos = $('#todos'); // die todos ul liste
    let input = $('#todo-input')
    let button = $('#todo-button')

    // events
    button.on('click', function (event) {
        addTodo();
    });

    $(document).on('keyup', function (event) {
        if (event.code === 13) addTodo()
    })

    // functions
    function addTodo() {
        if (input.val() != "") {
            todosArray.push(input.val());
            setLocalStorage();
            input.val("")
            render()
        }
    }

    function render() {
        todos.html("")
        for (let i = 0; i < todosArray.length; i++) {
            let style = ""
            if (i == todosArray.length - 1) style = "display: none"
            let li = $('<li style="' + style + '">' + todosArray[i] + '</li>');
            todos.append(li)
            li.fadeIn()
        }
    }
    render()


    function getLocalStorage() {
        return localStorage.getItem("Model") ? JSON.parse(localStorage.getItem("Model")) : [];
    }

    function setLocalStorage() {
        localStorage.setItem("Model", JSON.stringify(todosArray));
    }
});