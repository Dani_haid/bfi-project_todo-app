/* lädt das Modul laravel-mix */
let mix = require('laravel-mix');

/* macht aus der app.js eine neue Datei in Public "main.js" */
mix.js('src/app.js', 'public/main.js');

/* macht aus der styles.scss eine neue Datei in Public "styles.css"*/
mix.sass('src/styles.scss', 'public');